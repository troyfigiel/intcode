package interpreter

import (
	"os"
	"strconv"
	"strings"
)

func ReadIntcodeFile(path string) []int {
	content, _ := os.ReadFile(path)

	s := string(content)
	s = strings.ReplaceAll(s, "\n", "")
	s = strings.ReplaceAll(s, " ", "")
	sl := strings.Split(s, ",")

	memory := make([]int, len(sl))
	for i := range sl {
		memory[i], _ = strconv.Atoi(sl[i])
	}

	return memory
}
