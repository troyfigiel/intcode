package interpreter

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReadIntcodeFile(t *testing.T) {
	memory := ReadIntcodeFile("fixtures/example.txt")
	assert.Equal(
		t,
		[]int{109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99},
		memory,
	)
}
