package interpreter

type Program struct {
	Memory  []int
	Input   chan int
	Output  chan int
	pointer int
	base    int
}

func (p *Program) Interpret() {
	var halted bool

	for !halted {
		instruction := p.Memory[p.pointer]
		halted = p.step(instruction)
	}
}
