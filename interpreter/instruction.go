package interpreter

type opcode int

const (
	opcodeAdd          opcode = 1
	opcodeMultiply            = 2
	opcodeInput               = 3
	opcodeOutput              = 4
	opcodeJumpIfTrue          = 5
	opcodeJumpIfFalse         = 6
	opcodeLessThan            = 7
	opcodeEquals              = 8
	opcodeRelativeBase        = 9
	opcodeHalt                = 99
)

type mode int

const (
	modePosition  mode = 0
	modeImmediate      = 1
	modeRelative       = 2
)

func (p *Program) step(instruction int) bool {
	modeOperand3 := mode(instruction / 10000)
	modeOperand2 := mode((instruction % 10000) / 1000)
	modeOperand1 := mode((instruction % 1000) / 100)
	opcodeValue := opcode(instruction % 100)

	switch opcodeValue {

	case opcodeHalt:
		close(p.Output)
		return true

	case opcodeInput:
		p.write(1, modeOperand1, <-p.Input)
		p.pointer += 2

	case opcodeOutput:
		read1, _ := p.read(1, modeOperand1)
		p.Output <- read1
		p.pointer += 2

	case opcodeAdd:
		read1, _ := p.read(1, modeOperand1)
		read2, _ := p.read(2, modeOperand2)
		p.write(3, modeOperand3, read1+read2)
		p.pointer += 4

	case opcodeMultiply:
		read1, _ := p.read(1, modeOperand1)
		read2, _ := p.read(2, modeOperand2)
		p.write(3, modeOperand3, read1*read2)
		p.pointer += 4

	case opcodeJumpIfTrue:
		read1, _ := p.read(1, modeOperand1)
		read2, _ := p.read(2, modeOperand2)

		if read1 != 0 {
			p.pointer = read2
		} else {
			p.pointer += 3
		}

	case opcodeJumpIfFalse:
		read1, _ := p.read(1, modeOperand1)
		read2, _ := p.read(2, modeOperand2)

		if read1 == 0 {
			p.pointer = read2
		} else {
			p.pointer += 3
		}

	case opcodeLessThan:
		read1, _ := p.read(1, modeOperand1)
		read2, _ := p.read(2, modeOperand2)

		var result int
		if read1 < read2 {
			result = 1
		}

		p.write(3, modeOperand3, result)
		p.pointer += 4

	case opcodeEquals:
		read1, _ := p.read(1, modeOperand1)
		read2, _ := p.read(2, modeOperand2)

		var result int
		if read1 == read2 {
			result = 1
		}

		p.write(3, modeOperand3, result)
		p.pointer += 4

	case opcodeRelativeBase:
		read1, _ := p.read(1, modeOperand1)
		p.base += read1
		p.pointer += 2

	default:
		panic("Proper error handling not yet implemented!")
	}
	return false
}

func (p *Program) read(offsetFromPointer int, mode mode) (int, error) {
	var (
		base        int
		readPointer int = p.pointer + offsetFromPointer
		readFrom    int
	)

	readFrom = p.Memory[readPointer]

	switch mode {
	case modeImmediate:
		return readFrom, nil
	case modeRelative:
		base = p.base
	case modePosition:
	default:
		panic("Proper error handling not yet implemented!")
	}

	// All memory not explicitly defined in our slice should be
	// initialized as zero for Intcode.
	if readFrom >= len(p.Memory) {
		return 0, nil
	}

	return p.Memory[readFrom+base], nil
}

func (p *Program) write(offsetFromPointer int, mode mode, value int) {
	var (
		base         int
		writePointer int = p.pointer + offsetFromPointer
		writeTo      int
	)

	switch mode {
	case modePosition:
	case modeRelative:
		base = p.base
	default:
		panic("Proper error handling not yet implemented!")
	}

	writeTo = p.Memory[writePointer] + base
	for writeTo >= len(p.Memory) {
		p.Memory = append(p.Memory, 0)
	}
	p.Memory[writeTo] = value
}
