package interpreter

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testCaseFull struct {
	memory         []int
	input          []int
	expectedMemory []int
	expectedOutput []int
}

type testCaseIO struct {
	memory         []int
	input          []int
	expectedOutput []int
}

type testCaseMemory struct {
	memory         []int
	expectedMemory []int
}

func testProgramFull(t *testing.T, tests []testCaseFull) {
	for _, tt := range tests {
		p := Program{
			Memory: tt.memory,
			Input:  make(chan int),
			Output: make(chan int),
		}

		go p.Interpret()

		for _, value := range tt.input {
			p.Input <- value
		}

		actualOutput := []int{}
		for value := range p.Output {
			actualOutput = append(actualOutput, value)
		}

		message := fmt.Sprintln("program:", tt.memory)
		assert.Equal(t, tt.expectedOutput, actualOutput, message)
		assert.Equal(t, tt.expectedMemory, p.Memory, message)
	}
}

func testProgramIO(t *testing.T, tests []testCaseIO) {
	for _, tt := range tests {
		p := Program{
			Memory: tt.memory,
			Input:  make(chan int),
			Output: make(chan int),
		}

		go p.Interpret()

		for _, value := range tt.input {
			p.Input <- value
		}

		actualOutput := []int{}
		for value := range p.Output {
			actualOutput = append(actualOutput, value)
		}

		message := fmt.Sprintln("program:", tt.memory)
		assert.Equal(t, tt.expectedOutput, actualOutput, message)
	}
}

func testProgramMemory(t *testing.T, tests []testCaseMemory) {
	for _, tt := range tests {
		p := Program{
			Memory: tt.memory,
			Input:  make(chan int),
			Output: make(chan int),
		}

		go p.Interpret()

		for range p.Output {
		}

		message := fmt.Sprintln("program:", tt.memory)
		assert.Equal(t, tt.expectedMemory, p.Memory, message)
	}
}

func TestArithmetic(t *testing.T) {
	testProgramMemory(t,
		[]testCaseMemory{
			{
				memory:         []int{1, 0, 0, 0, 99},
				expectedMemory: []int{2, 0, 0, 0, 99},
			},
			{
				memory:         []int{1, 1, 0, 0, 99},
				expectedMemory: []int{2, 1, 0, 0, 99},
			},
			{
				memory:         []int{1, 4, 0, 0, 99},
				expectedMemory: []int{100, 4, 0, 0, 99},
			},
			{
				memory:         []int{1, 0, 4, 0, 99},
				expectedMemory: []int{100, 0, 4, 0, 99},
			},
			{
				memory:         []int{1, 0, 4, 3, 99},
				expectedMemory: []int{1, 0, 4, 100, 99},
			},
			{
				memory:         []int{1, 0, 1, 4, 99, 3, 3, 0, 99},
				expectedMemory: []int{8, 0, 1, 4, 1, 3, 3, 0, 99},
			},
			{
				memory:         []int{2, 3, 0, 3, 99},
				expectedMemory: []int{2, 3, 0, 6, 99},
			},
			{
				memory:         []int{2, 4, 4, 5, 99, 0},
				expectedMemory: []int{2, 4, 4, 5, 99, 9801},
			},
			{
				memory:         []int{1, 1, 1, 4, 99, 5, 6, 0, 99},
				expectedMemory: []int{30, 1, 1, 4, 2, 5, 6, 0, 99},
			},
			{
				memory:         []int{101, 5, 0, 0, 99},
				expectedMemory: []int{106, 5, 0, 0, 99},
			},
			{
				memory:         []int{1002, 4, 3, 4, 33},
				expectedMemory: []int{1002, 4, 3, 4, 99},
			},
			{
				memory:         []int{1101, 100, -1, 4, 0},
				expectedMemory: []int{1101, 100, -1, 4, 99},
			},
		},
	)
}

func TestIO(t *testing.T) {
	testProgramFull(t,
		[]testCaseFull{
			{
				memory:         []int{3, 0, 99},
				input:          []int{32},
				expectedMemory: []int{32, 0, 99},
				expectedOutput: []int{},
			},
			{
				memory:         []int{3, 1, 99},
				input:          []int{32},
				expectedMemory: []int{3, 32, 99},
				expectedOutput: []int{},
			},
			{
				memory:         []int{3, 0, 3, 1, 99},
				input:          []int{32, 57},
				expectedMemory: []int{32, 57, 3, 1, 99},
				expectedOutput: []int{},
			},
			{
				memory:         []int{4, 0, 4, 1, 99},
				input:          []int{},
				expectedMemory: []int{4, 0, 4, 1, 99},
				expectedOutput: []int{4, 0},
			},
			{
				memory:         []int{3, 0, 4, 0, 99},
				input:          []int{-29},
				expectedMemory: []int{-29, 0, 4, 0, 99},
				expectedOutput: []int{-29},
			},
			{
				memory:         []int{104, 0, 99},
				expectedMemory: []int{104, 0, 99},
				expectedOutput: []int{0},
			},
		},
	)
}

func TestJump(t *testing.T) {
	testProgramMemory(t,
		[]testCaseMemory{
			{
				memory:         []int{5, 6, 4, 1, 7, 7, 0, 99},
				expectedMemory: []int{198, 6, 4, 1, 7, 7, 0, 99},
			},
			{
				memory:         []int{5, 1, 4, 1, 7, 7, 0, 99},
				expectedMemory: []int{5, 1, 4, 1, 7, 7, 0, 99},
			},
			{
				memory:         []int{6, 6, 4, 1, 7, 7, 0, 99},
				expectedMemory: []int{6, 6, 4, 1, 7, 7, 0, 99},
			},
			{
				memory:         []int{6, 1, 4, 1, 7, 7, 0, 99},
				expectedMemory: []int{198, 1, 4, 1, 7, 7, 0, 99},
			},
			{
				memory:         []int{105, 0, 4, 1, 7, 7, 0, 99},
				expectedMemory: []int{198, 0, 4, 1, 7, 7, 0, 99},
			},
			{
				memory:         []int{105, -23, 4, 1, 7, 7, 0, 99},
				expectedMemory: []int{105, -23, 4, 1, 7, 7, 0, 99},
			},
			{
				memory:         []int{1005, 2, 7, 1, 7, 7, 0, 99},
				expectedMemory: []int{1005, 2, 7, 1, 7, 7, 0, 99},
			},
			{
				memory:         []int{1105, 0, 7, 1, 7, 7, 0, 99},
				expectedMemory: []int{198, 0, 7, 1, 7, 7, 0, 99},
			},
		},
	)
}

func TestCompare(t *testing.T) {
	testProgramMemory(t,
		[]testCaseMemory{
			{
				memory:         []int{7, 3, 0, 0, 99},
				expectedMemory: []int{1, 3, 0, 0, 99},
			},
			{
				memory:         []int{7, 0, 3, 0, 99},
				expectedMemory: []int{0, 0, 3, 0, 99},
			},
			{
				memory:         []int{8, 3, 1, 3, 99},
				expectedMemory: []int{8, 3, 1, 1, 99},
			},
			{
				memory:         []int{8, 3, 2, 3, 99},
				expectedMemory: []int{8, 3, 2, 0, 99},
			},
			{
				memory:         []int{107, 120, 0, 0, 99},
				expectedMemory: []int{0, 120, 0, 0, 99},
			},
			{
				memory:         []int{1007, 1, 3, 0, 99},
				expectedMemory: []int{1, 1, 3, 0, 99},
			},
		},
	)
}

func TestRelativeBaseArithmetic(t *testing.T) {
	testProgramMemory(t,
		[]testCaseMemory{
			{
				memory:         []int{9, 5, 1, 3, 6, 1, 99, 1},
				expectedMemory: []int{9, 102, 1, 3, 6, 1, 99, 1},
			},
			{
				memory:         []int{9, 5, 201, 3, 6, 1, 99, 1},
				expectedMemory: []int{9, 105, 201, 3, 6, 1, 99, 1},
			},
			{
				memory:         []int{9, 5, 2001, 3, 6, 1, 99, 1},
				expectedMemory: []int{9, 4, 2001, 3, 6, 1, 99, 1},
			},
			{
				memory:         []int{9, 4, 20001, 3, 6, 1, 99, 1},
				expectedMemory: []int{9, 4, 20001, 3, 6, 1, 99, 102},
			},
			{
				memory:         []int{9, 5, 2, 3, 6, 1, 99, 1},
				expectedMemory: []int{9, 297, 2, 3, 6, 1, 99, 1},
			},
			{
				memory:         []int{9, 5, 202, 3, 6, 1, 99, 1},
				expectedMemory: []int{9, 594, 202, 3, 6, 1, 99, 1},
			},
			{
				memory:         []int{9, 5, 2002, 3, 6, 1, 99, 1},
				expectedMemory: []int{9, 3, 2002, 3, 6, 1, 99, 1},
			},
			{
				memory:         []int{9, 4, 20002, 3, 6, 1, 99, 1},
				expectedMemory: []int{9, 4, 20002, 3, 6, 1, 99, 297},
			},
		},
	)
}

func TestRelativeBaseJump(t *testing.T) {
	testProgramMemory(t,
		[]testCaseMemory{
			{
				memory:         []int{9, 5, 5, 5, 0, 1, 0, 0, 0, 99},
				expectedMemory: []int{9, 5, 5, 5, 0, 1, 0, 0, 0, 99},
			},
			{
				memory:         []int{9, 5, 205, 5, 0, 1, 0, 0, 0, 99},
				expectedMemory: []int{18, 5, 205, 5, 0, 1, 0, 0, 0, 99},
			},
			{
				memory:         []int{9, 5, 2005, 5, -1, 1, 0, 0, 0, 99},
				expectedMemory: []int{9, 5, 2005, 5, -1, 1, 0, 0, 0, 99},
			},
			{
				memory:         []int{9, 5, 6, 5, 0, 1, 0, 0, 0, 99},
				expectedMemory: []int{18, 5, 6, 5, 0, 1, 0, 0, 0, 99},
			},
			{
				memory:         []int{9, 5, 206, 5, 0, 1, 0, 0, 0, 99},
				expectedMemory: []int{9, 5, 206, 5, 0, 1, 0, 0, 0, 99},
			},
			{
				memory:         []int{9, 5, 2006, 5, -1, 1, 0, 0, 0, 99},
				expectedMemory: []int{18, 5, 2006, 5, -1, 1, 0, 0, 0, 99},
			},
		},
	)
}

func TestRelativeBaseComparison(t *testing.T) {
	testProgramMemory(t,
		[]testCaseMemory{
			{
				memory:         []int{9, 5, 7, 3, 6, 1, 99, 1},
				expectedMemory: []int{9, 1, 7, 3, 6, 1, 99, 1},
			},
			{
				memory:         []int{9, 5, 2207, 3, 6, 1, 99, 1},
				expectedMemory: []int{9, 0, 2207, 3, 6, 1, 99, 1},
			},
			{
				memory:         []int{9, 1, 20007, 3, 6, 1, 99, 1},
				expectedMemory: []int{9, 1, 1, 3, 6, 1, 99, 1},
			},
			{
				memory:         []int{9, 5, 8, 4, 6, 1, 99, 1},
				expectedMemory: []int{9, 0, 8, 4, 6, 1, 99, 1},
			},
			{
				memory:         []int{9, 5, 2208, 4, 6, 1, 99, 1},
				expectedMemory: []int{9, 1, 2208, 4, 6, 1, 99, 1},
			},
			{
				memory:         []int{9, 5, 20008, 4, 6, 1, 99, 1},
				expectedMemory: []int{9, 5, 0, 4, 6, 1, 99, 1},
			},
		},
	)
}

func TestRelativeBaseIO(t *testing.T) {
	testProgramFull(t,
		[]testCaseFull{
			{
				memory:         []int{9, 1, 3, 0, 99},
				input:          []int{32},
				expectedMemory: []int{32, 1, 3, 0, 99},
				expectedOutput: []int{},
			},
			{
				memory:         []int{9, 1, 203, 0, 99},
				input:          []int{32},
				expectedMemory: []int{9, 32, 203, 0, 99},
				expectedOutput: []int{},
			},
			{
				memory:         []int{9, 1, 4, 0, 99},
				input:          []int{},
				expectedMemory: []int{9, 1, 4, 0, 99},
				expectedOutput: []int{9},
			},
			{
				memory:         []int{9, 1, 204, 0, 99},
				input:          []int{},
				expectedMemory: []int{9, 1, 204, 0, 99},
				expectedOutput: []int{1},
			},
		},
	)
}

// TODO: Create a test to check if the relative base is set correctly directly.
func TestSetRelativeBase(t *testing.T) {
	testProgramFull(t,
		[]testCaseFull{
			{
				memory:         []int{9, 1, 203, 0, 99},
				input:          []int{32},
				expectedMemory: []int{9, 32, 203, 0, 99},
				expectedOutput: []int{},
			},
			{
				memory:         []int{109, 2, 203, 1, 99},
				input:          []int{32},
				expectedMemory: []int{109, 2, 203, 32, 99},
				expectedOutput: []int{},
			},
			{
				memory:         []int{109, 2, 209, 1, 203, 0, 99},
				input:          []int{32},
				expectedMemory: []int{109, 2, 209, 32, 203, 0, 99},
				expectedOutput: []int{},
			},
		},
	)
}

func TestPositiveReadOutOfBoundsIsZero(t *testing.T) {
	testProgramMemory(t,
		[]testCaseMemory{
			{
				memory:         []int{1, 78, 1, 2, 99},
				expectedMemory: []int{1, 78, 78, 2, 99},
			},
			{
				memory:         []int{9, 10, 201, 78, 1, 2, 99},
				expectedMemory: []int{9, 10, 10, 78, 1, 2, 99},
			},
		},
	)
}

func TestPositiveWriteOutOfBoundsIsPossible(t *testing.T) {
	testProgramFull(t,
		[]testCaseFull{
			{
				memory:         []int{1, 0, 0, 7, 99},
				input:          []int{},
				expectedMemory: []int{1, 0, 0, 7, 99, 0, 0, 2},
				expectedOutput: []int{},
			},
			{
				memory:         []int{2, 0, 3, 7, 99},
				input:          []int{},
				expectedMemory: []int{2, 0, 3, 7, 99, 0, 0, 14},
				expectedOutput: []int{},
			},
			{
				memory:         []int{7, 0, 3, 8, 99},
				input:          []int{},
				expectedMemory: []int{7, 0, 3, 8, 99, 0, 0, 0, 1},
				expectedOutput: []int{},
			},
			{
				memory:         []int{8, 0, 3, 8, 99},
				input:          []int{},
				expectedMemory: []int{8, 0, 3, 8, 99, 0, 0, 0, 1},
				expectedOutput: []int{},
			},
			{
				memory:         []int{3, 7, 99},
				input:          []int{32},
				expectedMemory: []int{3, 7, 99, 0, 0, 0, 0, 32},
				expectedOutput: []int{},
			},
		},
	)
}

func TestExampleArithmetic(t *testing.T) {
	testProgramMemory(t,
		[]testCaseMemory{
			{
				memory:         []int{1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50},
				expectedMemory: []int{3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50},
			},
		},
	)
}

// TODO: From which AoC problem do these come?
func TestExampleCompareWithIO(t *testing.T) {
	testProgramIO(t,
		[]testCaseIO{
			{
				memory:         []int{3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8},
				input:          []int{8},
				expectedOutput: []int{1},
			},
			{
				memory:         []int{3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8},
				input:          []int{8},
				expectedOutput: []int{0},
			},
			{
				memory:         []int{3, 3, 1108, -1, 8, 3, 4, 3, 99},
				input:          []int{8},
				expectedOutput: []int{1},
			},
			{
				memory:         []int{3, 3, 1107, -1, 8, 3, 4, 3, 99},
				input:          []int{7},
				expectedOutput: []int{1},
			},
			{
				memory: []int{
					3, 12, 6, 12, 15, 1, 13, 14, 13, 4,
					13, 99, -1, 0, 1, 9,
				},
				input:          []int{3},
				expectedOutput: []int{1},
			},
			{
				memory: []int{
					3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4,
					12, 99, 1,
				},
				input:          []int{0},
				expectedOutput: []int{0},
			},
			{
				memory: []int{
					3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107,
					8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98,
					0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1,
					46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20,
					4, 20, 1105, 1, 46, 98, 99,
				},
				input:          []int{6},
				expectedOutput: []int{999},
			},
			{
				memory: []int{
					3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107,
					8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98,
					0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1,
					46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20,
					4, 20, 1105, 1, 46, 98, 99,
				},
				input:          []int{8},
				expectedOutput: []int{1000},
			},
			{
				memory: []int{
					3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107,
					8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98,
					0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1,
					46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20,
					4, 20, 1105, 1, 46, 98, 99,
				},
				input:          []int{10},
				expectedOutput: []int{1001},
			},
		},
	)
}

// TODO: From which AoC problem do these come?
func TestAoCProblems(t *testing.T) {
	testProgramMemory(t,
		[]testCaseMemory{
			{
				memory: []int{
					1, 0, 0, 3, 1, 1, 2, 3, 1, 3,
					4, 3, 1, 5, 0, 3, 2, 9, 1, 19,
					1, 5, 19, 23, 2, 9, 23, 27, 1, 27,
					5, 31, 2, 31, 13, 35, 1, 35, 9, 39,
					1, 39, 10, 43, 2, 43, 9, 47, 1, 47,
					5, 51, 2, 13, 51, 55, 1, 9, 55, 59,
					1, 5, 59, 63, 2, 6, 63, 67, 1, 5,
					67, 71, 1, 6, 71, 75, 2, 9, 75, 79,
					1, 79, 13, 83, 1, 83, 13, 87, 1, 87,
					5, 91, 1, 6, 91, 95, 2, 95, 13, 99,
					2, 13, 99, 103, 1, 5, 103, 107, 1, 107,
					10, 111, 1, 111, 13, 115, 1, 10, 115, 119,
					1, 9, 119, 123, 2, 6, 123, 127, 1, 5,
					127, 131, 2, 6, 131, 135, 1, 135, 2, 139,
					1, 139, 9, 0, 99, 2, 14,
				},
				expectedMemory: []int{
					250673, 0, 0, 2, 1, 1, 2, 3, 1, 3,
					4, 3, 1, 5, 0, 3, 2, 9, 1, 0,
					1, 5, 19, 1, 2, 9, 23, 3, 1, 27,
					5, 4, 2, 31, 13, 20, 1, 35, 9, 23,
					1, 39, 10, 27, 2, 43, 9, 81, 1, 47,
					5, 82, 2, 13, 51, 410, 1, 9, 55, 413,
					1, 5, 59, 414, 2, 6, 63, 828, 1, 5,
					67, 829, 1, 6, 71, 831, 2, 9, 75, 2493,
					1, 79, 13, 2498, 1, 83, 13, 2503, 1, 87,
					5, 2504, 1, 6, 91, 2506, 2, 95, 13, 12530,
					2, 13, 99, 62650, 1, 5, 103, 62651, 1, 107,
					10, 62655, 1, 111, 13, 62660, 1, 10, 115, 62664,
					1, 9, 119, 62667, 2, 6, 123, 125334, 1, 5,
					127, 125335, 2, 6, 131, 250670, 1, 135, 2, 250670,
					1, 139, 9, 0, 99, 2, 14,
				},
			},
		},
	)
}

func TestExamples(t *testing.T) {
	testProgramIO(t,
		[]testCaseIO{
			{
				memory:         []int{1102, 34915192, 34915192, 7, 4, 7, 99, 0},
				input:          []int{},
				expectedOutput: []int{1219070632396864},
			},
			{
				memory:         []int{104, 1125899906842624, 99},
				input:          []int{},
				expectedOutput: []int{1125899906842624},
			},
		},
	)
}

func TestAddTwoInputs(t *testing.T) {
	testProgramIO(t,
		[]testCaseIO{
			{
				memory:         ReadIntcodeFile("./fixtures/add_two_inputs.txt"),
				input:          []int{54, 21},
				expectedOutput: []int{75},
			},
			{
				memory:         ReadIntcodeFile("./fixtures/add_two_inputs.txt"),
				input:          []int{-15, 120},
				expectedOutput: []int{105},
			},
		},
	)
}

func TestMultiplyTwoInputs(t *testing.T) {
	testProgramIO(t,
		[]testCaseIO{
			{
				memory:         ReadIntcodeFile("./fixtures/multiply_two_inputs.txt"),
				input:          []int{54, 21},
				expectedOutput: []int{1134},
			},
			{
				memory:         ReadIntcodeFile("./fixtures/multiply_two_inputs.txt"),
				input:          []int{-15, 120},
				expectedOutput: []int{-1800},
			},
		},
	)
}

func TestQuine(t *testing.T) {
	testProgramIO(t,
		[]testCaseIO{
			{
				memory:         ReadIntcodeFile("./fixtures/quine.txt"),
				input:          []int{},
				expectedOutput: ReadIntcodeFile("./fixtures/quine.txt"),
			},
		},
	)
}

func TestCompareToEight(t *testing.T) {
	testProgramIO(t,
		[]testCaseIO{
			{
				memory:         ReadIntcodeFile("./fixtures/compare_to_eight.txt"),
				input:          []int{-43},
				expectedOutput: []int{999},
			},
			{
				memory:         ReadIntcodeFile("./fixtures/compare_to_eight.txt"),
				input:          []int{7},
				expectedOutput: []int{999},
			},
			{
				memory:         ReadIntcodeFile("./fixtures/compare_to_eight.txt"),
				input:          []int{8},
				expectedOutput: []int{1000},
			},
			{
				memory:         ReadIntcodeFile("./fixtures/compare_to_eight.txt"),
				input:          []int{9},
				expectedOutput: []int{1001},
			},
			{
				memory:         ReadIntcodeFile("./fixtures/compare_to_eight.txt"),
				input:          []int{32},
				expectedOutput: []int{1001},
			},
		},
	)
}

func TestAdventOfCode(t *testing.T) {
	testProgramIO(t,
		[]testCaseIO{
			{
				memory:         ReadIntcodeFile("./fixtures/aoc/input_05.txt"),
				input:          []int{1},
				expectedOutput: []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 13933662},
			},
			{
				memory:         ReadIntcodeFile("./fixtures/aoc/input_05.txt"),
				input:          []int{5},
				expectedOutput: []int{2369720},
			},
			{
				memory:         ReadIntcodeFile("./fixtures/aoc/input_09_part_1.txt"),
				input:          []int{1},
				expectedOutput: []int{2714716640},
			},
			{
				memory:         ReadIntcodeFile("./fixtures/aoc/input_09_part_2.txt"),
				input:          []int{2},
				expectedOutput: []int{58879},
			},
		},
	)
}
