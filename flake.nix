{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:nixos/nixpkgs";
  };

  outputs = inputs:
    let inherit (inputs.flake-utils.lib) eachSystem system;
    in eachSystem (with system; [ x86_64-linux aarch64-linux ]) (system: rec {
      pkgs = import inputs.nixpkgs { inherit system; };
      devShells.default =
        pkgs.mkShell { buildInputs = with pkgs; [ go gopls ]; };
    });
}
